#import the package
from flask import Flask, render_template
app = Flask(__name__)

#defining the route
@app.route("/")
def hello(chartID = 'chart_ID', chart_type = 'line', chart_height = 350):
	chart = {"renderTo": chartID, "type": chart_type, "height": chart_height,}
	title = {"text": 'Live random data'}
	xAxis = {"categories": []}
	yAxis = {"title": {"text": 'pH'}}
	series = [{"name": 'Random data', "data": []}]
	return render_template("hello.html", chartID=chartID, chart=chart, title=title, xAxis=xAxis, yAxis=yAxis, series=series)

#run the program
if __name__ == "__main__":
    app.run(debug=True)
