this project is to try to display live random generated data as graph/chart on webpage using flask.

the script does not give any error when run but there is no chart/graph being displayed on the webpage.

I create a new project just to try to display real time chart on the webpage hoping that if it works, I can try to modified it and add flask-socket function to the original script. However there is no chart/graph displayed on the webpage when I run the script.

The problem probably with the arrangement for the javascript in the .html file but I already rearrange it by putting the javascript inside the <head> block and also <body> block.

For other solution, I tried to put the javascript file inside folder static/js and renamed it to "random.js" and try to run the script by inserting the path to the javascript file in the .html file but that does not work either.